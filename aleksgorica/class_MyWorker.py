from class_Unit import Unit
import random
from lia import constants
import math
from lia import math_util
class MyWorker(Unit):
    def __init__(self, unit, state, api):
        super().__init__(unit, state, api)
        self.task = "SEARCH"#SEARCH, ATTACK, EAT, ESCAPE, PROTECT
        self.times = 10
        self.changeDirection = 0
        self.target = ()
        self.previousHealth = 100

    def protect(self, id):
        self.task = "PROTECT"
        pass

    def moveAway(self, api):
        unit = None
        if self.unit["opponentsInView"]:
            unit = self.unit["opponentsInView"][0]
        if unit != None:
            api.set_speed(self.id, "FORWARD")
            if math_util.angle_between_unit_and_point_using_parameters(self.unit["x"], self.unit["y"], self.unit["orientationAngle"], unit["x"], unit["y"]) < 0:
                    api.set_rotation(self.id, "RIGHT")
            elif math_util.angle_between_unit_and_point_using_parameters(self.unit["x"], self.unit["y"], self.unit["orientationAngle"], unit["x"], unit["y"]) > 0:
                api.set_rotation(self.id, "LEFT")
            else:
                api.set_rotation(self.id, "NONE")
        else: 
            return False

    def escape(self, api):
        if self.health < constants.UNIT_FULL_HEALTH - 50 and len(self.unit["opponentsInView"]) == 0 and self.changeDirection < 10:
            if round(math.floor(self.changeDirection / 10)) < 4:
                api.set_speed(self.id, "FORWARD")
                api.set_rotation(self.id, "LEFT")
                self.changeDirection += 1
            elif round(math.floor(self.changeDirection / 10)) < 8:
                api.set_speed(self.id, "FORWARD")
                api.set_rotation(self.id, "RIGHT")
                self.changeDirection += 1                
        else:
            self.changeDirection = 0
            self.task = "SEARCH" 

    def search(self, api):
        if self.health < self.previousHealth:
            if self.unit["navigationPath"]:
                self.knownResources.dontGoAfter((self.unit["navigationPath"][-1]["x"], self.unit["navigationPath"][-1]["y"]))
            self.task = "ESCAPE"
            self.changeDirection = 0
            return 0

        #if self.id == 0:
            # print("LEN NAVIGATION PATH", len(self.unit["navigationPath"]) == 0)
           # print("Navigation path", self.unit["navigationPath"])

        if self.knownResources.nearestFree(self.unit) != False:
            nearest = self.knownResources.nearestFree(self.unit)
            # print("going after", self.id, self.location, nearest)
            self.knownResources.goAfter(nearest, self)
            # if nearest != self.target:
            self.target = nearest            
            api.navigation_start(self.unit["id"], nearest[0], nearest[1])
            return 0

        if len(self.navigationPath) == 0:
            print("*******************************************************************************************")
            while True:
                x = random.randint(0, constants.MAP_WIDTH - 1)
                y = random.randint(0, constants.MAP_HEIGHT - 1)

                # If map[x][y] equals false it means that at (x,y) there is no obstacle.
                if constants.MAP[x][y] is False:
                    # Send the unit to (x, y)
                    # print(self.unit["id"], x, y)
                    api.navigation_start(self.unit["id"], x,y)
                    break
            return 0

        

        # elif len(self.unit["navigationPath"]) == 0:   
            
    def checkForResources(self):
        if len(self.unit["resourcesInView"]) > 0:
            for resource in self.unit["resourcesInView"]:
                self.knownResources.addResource((resource["x"], resource["y"]))
                
    def run(self, api):
        self.checkForResources()
        self.knownResources.deleteResource(self.location)
        #     api.navigation_stop(self.id)

        # if not self.moveAway(api):
        if self.task == "SEARCH":
            self.search(api)
        if self.task == "ESCAPE":
            self.escape(api)
        self.previousHealth = self.health


    def __str__(self):
        return str(self.id) + " " + str(self.navigationPath)
    # sb = []
    # for key in self.__dict__:
    #     sb.append("{key}='{value}'".format(key=key, value=self.__dict__[key]))

    # return ', '.join(sb)
 
    def __repr__(self):
        return self.__str__()