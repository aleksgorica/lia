class Opponent:
    def __init__(self, unit, visibleBy):
        self.id = unit["id"]
        self.location = (unit["x"], unit["y"]) #touple
        self.speed = unit["speed"]
        self.orientationAngle = unit["orientationAngle"]
        self.health = unit["health"]
        self.role = unit["type"]
        self.visibleBy = [visibleBy["id"]]

    def update(self, unit):
        self.id = unit["id"]
        self.location = (unit["x"], unit["y"]) #touple
        self.speed = unit["speed"]
        self.orientationAngle = unit["orientationAngle"]
        self.health = unit["health"]
        self.role = unit["type"]

    def visible(self, who):
        self.visibleBy = who

    def isWanted(self, wantedBy = None):
        self.wanted = wantedBy

    def unWanted(self):
        self.wanted = None
    
    
    def notVisible(self):
        self.visibleBy = []