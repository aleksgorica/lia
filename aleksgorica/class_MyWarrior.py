from class_Unit import Unit
from lia.api import *
import random
from lia import constants
from lia import math_util
import math

from util import *

class MyWarrior(Unit):
    def __init__(self, unit, state, api):
        super().__init__(unit, state, api)
        self.task = "SEARCH" #SEARCH, ATTACK, EAT, ESCAPE, PROTECT
        self.lost_enemy_time = 0
        self.inShooting = False
        self.direction = "LEFT"
        self.test = None
        self.ct = 0
        self.changeDirection = 0
        self.assasinMode = False
        self.spawning = None
        if unit["x"] < 100:
            self.spawning = "down"
        else:
            self.spawning = "upper"

    def protect(self, id):
        pass

    def align(self, unit, opponent, api):
        angle = math_util.angle_between_unit_and_point_using_parameters(unit["x"], unit["y"], unit["orientationAngle"], opponent.location[0], opponent.location[1])
        if math_util.distance(unit["x"], unit["y"], opponent.location[0], opponent.location[1]) < 10:
            api.set_speed(self.id, "NONE")
        else: 
            api.set_speed(self.id, "FORWARD")

        if abs(angle) < 3:
            if self.dontShoot(api):
                api.shoot(self.id)
                api.shoot(self.id)
                api.shoot(self.id)
            return True
        if angle < -1:
            return "RIGHT"
        if angle > 1: 
            return "LEFT"

    def firstShoot(self, unit, opponent):
        self.direction = None
        self.oldTime = None
        self.time = None
        self.oldTime = self.state["time"]
        opp_velocity = None

        dist = math_util.distance(unit["x"], unit["y"], opponent.location[0], opponent.location[1])
        opp_orientation = opponent.orientationAngle
        my_orientation = unit["orientationAngle"]

        if opponent.speed == "FORWARD":
            opp_velocity = constants.UNIT_FORWARD_VELOCITY
        elif opponent.speed == "BACKWARD":
            opp_velocity = constants.UNIT_BACKWARD_VELOCITY
            opp_orientation += 180
        else: 
            return "NOW"
        opp_k = math.tan(math.radians(opp_orientation))
        my_k = math.tan(math.radians(my_orientation))
        beta = math.atan(abs((opp_k - my_k) / (1 + opp_k * my_k)))
        result = bisection(-3.001, 3.001, 0.00001, dist, beta, opp_velocity)
        self.time =  abs(result)+self.oldTime
        print(self.time - self.oldTime)
        B = (opponent.location[0] + math.cos(math.radians(opp_orientation)), opponent.location[1] + math.sin(math.radians(opp_orientation)))

        if my_k * B[0] - my_k * unit["x"] + unit["y"] < B[1]:
            if opponent.location[0] < unit["x"]:  
                self.direction = "RIGHT"
            else:
                self.direction = "LEFT"

        else: 
            if opponent.location[0] < unit["x"]:
                self.direction = "LEFT"

            else:
                self.direction = "RIGHT"

    def letsShoot(self, api):
        shooting = self.firstShoot(self.unit, self.target)
        if shooting == "NOW":
            if self.dontShoot(api):
                api.shoot(self.unit["id"])
            self.task = "ATTACK"
            return 0
        if self.direction == "LEFT":
            if self.state["time"] > self.time:# + 0.8:
                print(self.time - self.oldTime)
                if self.dontShoot(api):
                    api.shoot(self.unit["id"])
                self.task = "ATTACK"
                return 0
            else:
                api.set_rotation(self.unit["id"], "LEFT")
                
        else:
            if self.state["time"] > self.time: #+ 0.8:
                print(self.time - self.oldTime)
                if self.dontShoot(api):
                    api.shoot(self.unit["id"])
                self.task = "ATTACK"
                return 0
            else:
                api.set_rotation(self.unit["id"], "RIGHT")
            

    def attack(self, api):
        if len(self.unit["opponentsInView"]) > 0:
            self.lost_enemy_time == 0
            whoToShoot = self.knownOpponents.whoToShoot(self.unit)
            self.target = whoToShoot
            align = self.align(self.unit, whoToShoot, api)
            if align == True:
                return 0
                
            elif align == "RIGHT":
                api.set_rotation(self.id, "RIGHT")
            elif align == "LEFT":
                api.set_rotation(self.id, "LEFT")
        elif not self.assasinMode == False:
            if self.lostEnemy(api):
                self.goToSpawn(api)
        elif self.lostEnemy(api):
            self.task = "SEARCH"
            

    def goToSpawn(self, api):
        if self.spawning == "down":
            self.x_goal = constants.MAP_WIDTH - 1
            self.y_goal = constants.MAP_HEIGHT - 1
            self.aim_x = 168.5
            self.aim_y = 91.5
        else:
            self.x_goal = 1
            self.y_goal = 1
            self.aim_x = 7.5
            self.aim_y = 7.5
        if math_util.distance(self.unit["x"], self.unit["y"], self.x_goal, self.y_goal) < 4:

            api.set_speed(self.id, "NONE")
            if math_util.angle_between_unit_and_point_using_parameters(self.unit["x"], self.unit["y"], self.unit["orientationAngle"], self.aim_x, self.aim_y) < -5:
                api.set_rotation(self.id, "RIGHT")
            elif math_util.angle_between_unit_and_point_using_parameters(self.unit["x"], self.unit["y"], self.unit["orientationAngle"], self.aim_x, self.aim_y) > 5:
                api.set_rotation(self.id, "LEFT")
            else:
                api.set_rotation(self.id, "NONE")
        else:
            api.navigation_start(self.id, self.x_goal, self.y_goal)

    def escape(self, api):
        if self.health < constants.UNIT_FULL_HEALTH - 30 and len(self.unit["opponentsInView"]) == 0:
            if round(math.floor(self.changeDirection / 10)) < 4:
                api.set_speed(self.id, "FORWARD")
                api.set_rotation(self.id, "LEFT")
                self.changeDirection += 1
            elif round(math.floor(self.changeDirection / 10)) < 8:
                api.set_speed(self.id, "FORWARD")
                api.set_rotation(self.id, "RIGHT")
                self.changeDirection += 1
            else: 
                self.changeDirection = 0
                
        else:
            self.task = "SEARCH" 


    def search(self, api):
        if self.assasinMode == True:
            self.goToSpawn(api)


        if len(self.unit["opponentsInView"]) > 0:
            self.lost_enemy_time == 0
            self.task = "ATTACK"
            return 0

        if self.health != constants.UNIT_FULL_HEALTH:
            self.changeDirection = 0
            self.task = "ESCAPE"
        
        assist = self.knownOpponents.goForHelp(self.unit) 
        if assist:
            if self.assasinMode == True and assist[1] < 30:
                api.navigation_start(self.id, assist[0].location[0], assist[0].location[1])
            elif self.assasinMode == False:
                api.navigation_start(self.id, assist[0].location[0], assist[0].location[1])


        #if self.id == 0:
            # print("LEN NAVIGATION PATH", len(self.unit["navigationPath"]) == 0)
           # print("Navigation path", self.unit["navigationPath"])
        if len(self.unit["navigationPath"]) == 0 and self.assasinMode == False:
            #print("IS len really 0")
            # Generate new x and y until you get a position on the map
            # where there is no obstacle.
    

            while True:
                x = random.randint(0, constants.MAP_WIDTH - 1)
                y = random.randint(0, constants.MAP_HEIGHT - 1)

                # If map[x][y] equals false it means that at (x,y) there is no obstacle.
                if constants.MAP[x][y] is False:
                    # Send the unit to (x, y)
                    # print(self.unit["id"], x, y)
                    api.navigation_start(self.unit["id"], x,y)
                    break
                # If the unit  is a worker and it sees at least one resource
                # then make it go to the first resource to collect it.
        
        # print(nearest)
        #     print("nearest")
            #if self.id == 0:
              # print(self.unit["navigationPath"])
               # print("my location", self.location[0], self.location[1])
               # print("Coordinates of resource", nearest[0], nearest[1])
                
    #      self.api.navigationStart(self.unit["id"], )
    def checkForResources(self):
        if len(self.unit["resourcesInView"]) > 0:
            for resource in self.unit["resourcesInView"]:
                self.knownResources.addResource((resource["x"], resource["y"]))

    def becomeAssasin(self, api):
        self.assasinMode = True

    def dontShoot(self, api):
        k = math.tan(self.orientationAngle)
        a = -k
        b = 1
        c = k*self.location[0] - self.location[1]
        for unit in self.state["units"]:
            angle = math_util.angle_between_unit_and_point_using_parameters(self.location[0], self.location[1], self.orientationAngle, unit["x"], unit["y"])
            something_between = False
            distance = math_util.distance(self.location[0], self.location[1], unit["x"], unit["y"])
            for opponent in self.unit["opponentsInView"]:
                o_d = abs(a*opponent["x"] + b * opponent["y"] + c) / ((a**2 + b**2) ** (1 / 2))
                distance_opp = math_util.distance(self.location[0], self.location[1], opponent["x"], opponent["y"])
                if o_d < constants.UNIT_DIAMETER and distance_opp < distance:
                    something_between = True
            # d = abs(a*unit["x"] + b * unit["y"] + c) / ((a**2 + b**2) ** (1 / 2))
            if distance < constants.BULLET_RANGE and abs(angle) < 90 and something_between == True:
                api.say_something(self.id, "Dont shoot")
                return False
        return True


    def lostEnemy(self, api):
        if self.lost_enemy_time == 3:
            self.task = "SEARCH"
            return True
        else: 
            api.set_rotation(self.id, "RIGHT")
            api.set_speed(self.id, "NONE")
            self.lost_enemy_time += 1
            return False
            
    def run(self, api):
        self.checkForResources()
        if self.task == "SEARCH":
            self.search(api)
        if self.task == "ESCAPE":
            self.escape(api)
        if self.task == "ATTACK":
            self.attack(api)
        if self.task == "SHOOT":
            self.letsShoot(api)
