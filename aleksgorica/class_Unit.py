from lia import constants
import random

class Unit:
    def __init__(self, unit, state, api):
        self.unit = unit
        self.id = unit["id"]
        self.location = (unit["x"], unit["y"]) #touple
        self.speed = unit["speed"]  #Forward
        self.orientationAngle = unit["orientationAngle"]
        self.health = unit["health"]
        self.role = unit["type"]
        self.rotation = unit["rotation"]
        self.state = state
        self.api = api
        self.locHist = []
        self.navigationPath = unit["navigationPath"]


    def update(self, unit, state, knownResources, knownOpponents):
        self.id = unit["id"]
        self.location = (unit["x"], unit["y"]) #touple
        self.speed = unit["speed"]  #Forward
        self.orientationAngle = unit["orientationAngle"]
        self.health = unit["health"]
        self.navigationPath = unit["navigationPath"]
        self.role = unit["type"]
        self.rotation = unit["rotation"]
        self.state = state
        self.unit = unit
        
        self.knownResources = knownResources
        self.knownOpponents = knownOpponents
        self.knownOpponents.update(unit["opponentsInView"], unit)

    def standingTooMuch(self, api):
        count = 0
        prev = None
        self.locHist.append((self.unit["x"], self.unit["y"]))
        if len(self.locHist) > 50:
            self.locHist.pop(0)
        for e in self.locHist:
            if e == prev:
                count = count + 1
            elif e != prev:
                count = 0
            prev = e
        if count > 45:
            x = -1
            y = -1
            while True:
                while not 0 < x < 175:
                    x = round(prev[0]) + random.randint(-3, 3)
                while not 0 < y < 99:
                    y = round(prev[0]) + random.randint(-3, 3)        

                if constants.MAP[x][y] is False:
                    api.navigation_start(self.unit["id"], x,y)
                    break
            return True
                    
        else:
            return False

