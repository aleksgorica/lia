from class_Resource import Resource
from lia import math_util
from lia import constants

class KnownResources:
    def __init__(self, resource = None):
        self.resources = []
        self.free = []

    def addResource(self, location):
        if len(self.resources) > 0:
            for resource in self.resources:
                if resource.location == location:
                    return False
            else:
                self.resources.append(Resource(location))
                return True
        else:
            self.resources.append(Resource(location))
            return True

    def deleteResource(self, location):
        for resource in self.resources:            
            if constants.UNIT_DIAMETER + 0.001 > (((resource.location[0]-location[0])**2 + (resource.location[1]-location[1])**2)**(1/2)):
                print("deleted resource", resource.location)
                self.resources.remove(resource)

    def goAfter(self, location, unit):
        for resource in self.resources:
            if resource.location == location:
                if resource.wanted != None:
                    return False
                else: 
                    resource.isWanted(unit)
                    return True

    def dontGoAfter(self, location):
        for resource in self.resources:
            if resource.location == location:
                resource.unWanted()
                return True
        else:
            return False

    def checkGoing(self, workers):
        for resource in self.resources:
            good_r = False  
            for worker in workers:
                # print("\n", worker, "\n")
                if worker.target:
                    x = abs(worker.target[1] - resource.location[0])
                    y = abs(worker.target[1] - resource.location[1])

                    if x**2 + y**2 < constants.UNIT_DIAMETER ** 2 :
                        resource.wanted = worker
                        good_r = True
            if good_r == False:
                resource.unWanted()
                print("Resource unwanted", resource.location)

            


    def nearestFree(self, unit):
        best = 100000000
        coor = ()
        ress = {
            "free": []
        }
        if self.resources:
            for resource in self.resources:
                
                if resource.wanted == None:
                    ress["free"].append(resource.location)
                    rx = resource.location[0]
                    ry = resource.location[1]
                    ux = unit["x"]
                    uy = unit["y"]
                    d = ((rx-ux)**2 + (ry-uy)**2)**(1/2)
                    if d < best:
                        best = d
                        coor = (rx, ry) 
                elif resource.wanted.id == unit["id"]:
                    # print("unit", unit["id"], "wants", resource.location)
                    ress["free"].append(resource.location)
                    rx = resource.location[0]
                    ry = resource.location[1]
                    ux = unit["x"]
                    uy = unit["y"]
                    d = ((rx-ux)**2 + (ry-uy)**2)**(1/2)
                    if d < best:
                        best = d
                        coor = (rx, ry) 
                    # return resource.location

                else:   
                    if not resource.wanted.id in ress:
                        ress[resource.wanted.id] = [resource.location]
                    else:
                        ress[resource.wanted.id].append(resource.location)
        #print("\n ress", ress, "\n")
        print("ress",ress)
        if coor == ():
            return False
        return coor
