from class_Unit import Unit
import random
from lia import constants
from lia import math_util
import math

def bisection(a, b, tol, dist, beta, v):
    xl = a
    xr = b
    c = None
    while (abs(xl-xr) >= tol):
        if xl == 0:
            xl = 0.0000001
        if xr == 0:
            xr = 0.0000001
        c = (xl + xr) / 2.0
        if c == 0:
            c = 0.0000001
        prod = f(xl, beta, dist, v) * f(c, beta, dist, v)
        if prod > tol:
            xl = c
        else:
            if prod < tol:
                xr = c
    return c

def f(t, beta, d, unit_speed):
    beta = beta
    bullet_speed = constants.BULLET_VELOCITY
    angularVelocity = math.radians(constants.UNIT_ROTATION_VELOCITY)
    d = d
    unit_speed = unit_speed
    
    a = math.sin(math.radians(180) - angularVelocity * t - beta) / d
    b = math.sin(beta) / bullet_speed
    c = unit_speed * t * math.sin(beta) / (bullet_speed * math.sin(angularVelocity * t) - unit_speed * math.sin(beta))
    y = a - b / c
    return y
