from class_MyWorker import MyWorker
from lia.api import *
from class_KnownResources import KnownResources
from class_KnownOpponents import KnownOpponents
from class_MyWarrior import MyWarrior

class MyTeam:
    def __init__(self, state, api):
        self.state = state
        self.warriors = []
        self.workers = []
        self.assasins = []
        self.api = api
        for unit in self.state["units"]:
            if unit["type"] == "WARRIOR":
                self.warriors.append(MyWarrior(unit, self.state, api))
            if unit["type"] == "WORKER":
                self.workers.append(MyWorker(unit, self.state, api))   
        self.knownResources = KnownResources()
        self.knownOpponents = KnownOpponents()
    def addWarrior(self, warrior):
        self.api.spawnUnit("WARRIOR")

    def addWorker(self, worker):
        self.api.spawnUnit("WARRIOR")

            
    def makeAssasins(self, api):
        if len(self.warriors) > 2:
            if len(self.assasins) < 1:
                self.warriors[0].becomeAssasin(api)
                self.assasins.append(self.warriors[0])


   
    def checkWorkers(self, msg):
        for worker in self.workers:
            print(msg, worker.id, worker.location, worker.target)

    def update(self, state, api):
        print(state["time"], "\n")
        self.oldState = self.state
        self.state = state 
        self.knownOpponents.start()
        self.checkAliveUnits(api)
        self.makeAssasins(api)
        self.knownResources.checkGoing(self.workers)

        # self.checkWorkers("before updateUnits:")
        self.updateUnits(api)

        self.knownOpponents.end()
        # self.checkWorkers("before runUnits:   ")
        self.runUnits(api)
        # self.checkWorkers("after RunUnits:    ")


        # print("my_team.update after runUnits*********************", self.knownOpponents.opponents)


  
    def updateUnits(self, api):
        for unit in self.warriors:
            for i in self.state["units"]:
                if i["id"] == unit.id:
                    unit.update(i, self.state, self.knownResources, self.knownOpponents)
                    #if unit.standingTooMuch(api) == False:
                    break
        for unit in self.workers: 
            for i in self.state["units"]:
                if i["id"] == unit.id:
                    unit.update(i, self.state, self.knownResources, self.knownOpponents)
                    #if unit.standingTooMuch(api) == False:
                    break

    def runUnits(self, api):      
        for unit in self.warriors:
            for i in self.state["units"]:
                if i["id"] == unit.id:
                    unit.run(api)
                    break
        for unit in self.workers: 
            for i in self.state["units"]:
                if i["id"] == unit.id:
                    #if unit.standingTooMuch(api) == False:
                    unit.run(api)
                    break      

    def checkAliveUnits(self, api):
        old_ids = []
        new_ids = []
        for u in self.oldState["units"]:
            if not u["id"] in old_ids:
                old_ids.append(u["id"])

        for u in self.state["units"]:
            if not u["id"] in new_ids:
                new_ids.append(u["id"])

        for unit in self.state["units"]:
            if not unit["id"] in old_ids:
                if unit["type"] == "WARRIOR":
                    self.warriors.append(MyWarrior(unit, self.state, api))
                if unit["type"] == "WORKER":
                    self.workers.append(MyWorker(unit, self.state, api))   

        for unit in self.oldState["units"]:
            if not unit["id"] in new_ids:
                if unit["type"] == "WARRIOR":
                    id_ = unit["id"]
                    for i in self.warriors:
                        if i.id == id_:
                            self.warriors.remove(i)
                            break
                    for i in self.assasins:
                        if i.id == id_:
                            self.assasins.remove(i)
                            break
                if unit["type"] == "WORKER":
                    id_ = unit["id"]
                    for i in self.workers:
                        if i.id == id_:
                            self.workers.remove(i)
                            break

    
