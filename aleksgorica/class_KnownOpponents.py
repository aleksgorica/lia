from class_Opponent import Opponent
from lia import math_util
from lia import constants

class KnownOpponents:
    def __init__(self):
        self.opponents = []
        self.opponents_id = []
        self.visible = {} # (id, unit1, unit2, ...)
        self.diff_id = []

    def update(self, visibleOpponents, whoSaw):
        for v_opponent in visibleOpponents:
            if not v_opponent["id"] in self.opponents_id:
                self.opponents_id.append(v_opponent["id"])
                self.opponents.append(Opponent(v_opponent, whoSaw))
            if v_opponent["id"] in self.visible:
                self.visible[v_opponent["id"]].append(whoSaw["id"])
            else:
                self.visible[v_opponent["id"]] = [whoSaw["id"]]

            for opponent in self.opponents:
                if opponent.id == v_opponent["id"]:
                    opponent.update(v_opponent)

        for opponent in self.opponents:
            if not opponent.id in self.visible:
                self.diff_id.append(opponent.id)


        

    # def addOpponent(self, opp_unit, my_unit):
    #     knownIDs = []
    #     if len(self.opponents) > 0:
    #         for o in self.opponents:
    #             if not o.id in knownIDs:
    #                 knownIDs.append(o.id)
    #         if opp_unit["id"] in knownIDs:
    #             return False
            
    #         else:
    #             self.opponents.append(Opponent(opp_unit, my_unit))
    #             return True
    #     else:
    #         self.opponents.append(Opponent(opp_unit, my_unit))
    #         return True

    def killOpponent(self, location):
        pass
    def goAfter(self, location, unit):
        for opponent in self.opponents:
            if opponent.location == location:
                if opponent.wanted["id"] != unit["id"]:
                    return False
                else: 
                    opponent.wanted = unit
                    return True

    def dontGoAfter(self, location):
        for opponent in self.opponents:
            if opponent.location == location:
                opponent.wanted = None

    def goForHelp(self, unit):
        enemies = []
        nearest_enemy = None
        if self.opponents:
            for opponent in self.opponents:
                if opponent.visibleBy != []:
                    enemies.append(opponent)
                    for enemy in enemies:
                        d = math_util.distance(unit["x"], unit["y"], enemy.location[0], enemy.location[1])
                        if not nearest_enemy:
                            nearest_enemy = (enemy,d)
                        elif nearest_enemy[1] > d:
                            nearest_enemy = (enemy, d)
            return nearest_enemy
        else: 
            return False

    def whoToShoot(self, unit):
        warriors = []
        lowest_health_warrior = None
        workers = []
        lowest_health_worker = None
        if self.opponents:
            for opponent in self.opponents:
                if unit["id"] in opponent.visibleBy:
                    if opponent.role == "WARRIOR":
                        warriors.append(opponent)
                        for warrior in warriors:
                            if not lowest_health_warrior:
                                lowest_health_warrior = warrior
                            elif lowest_health_warrior.health > warrior.health:
                                lowest_health_warrior = warrior
                    if opponent.role == "WORKER":
                            workers.append(opponent)
                            for worker in workers:
                                if not lowest_health_worker:
                                    lowest_health_worker = worker
                                elif lowest_health_worker.health > worker.health:
                                    lowest_health_worker = worker
        if lowest_health_warrior == None:
            if lowest_health_worker == None:
                return False
            else:
                return lowest_health_worker
        else:
            return lowest_health_warrior
        
    def end(self):
        for opponent in self.opponents:
            if opponent.id in self.diff_id:
                opponent.notVisible()
            if opponent.id in self.visible:
                opponent.visible(self.visible[opponent.id])
        

    def start(self):
        self.visible = {}
        self.diff = []
